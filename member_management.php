<?php
session_start();
if(empty($_SESSION['username']) && empty($_SESSION['password']))
{
  header('Location: login.php');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">

  <style>
  #Kanit{
    font-family: 'Kanit', sans-serif;
  }
  #center{
    text-align: center;
  }
  .table tr:hover {background-color: #ddd;}
  .table th {
  padding-top: 12px;
  padding-bottom: 12px;
  background-color:#CC0000;
  color: white;
  }
  </style>
  <title>การจัดการผู้ใช้</title>
  <link rel="shortcut icon" href="photo/main-logo.png" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"></head>

  <body style="background-color:WhiteSmoke;"><br>
  <h2 align = 'center' id='Kanit' style="font-size:55px;"><u><b>การจัดการผู้ใช้</b></u></h2><br><br>
  <div align="center">
      <button type="submit" class="btn btn-success" id="Kanit" onclick = 'window.location.href="addMember_view.php"' style="text-align:center; height: 50px; width: 12   0px; font-size: 27px;" >เพิ่มผู้ใช้</button>

      &nbsp;&nbsp;&nbsp;&nbsp;

      <button type="button" class="btn btn-default" id="Kanit"
              onclick="window.location.href='index.php';" style="text-align:center; height: 50px; width: 150px; font-size: 27px; background-color: lightgrey">ย้อนกลับ</button>
  </div></br>

  <table class="table" border="2" id='Kanit' align = 'center' style="text-align:center;width:80%;background-color:white;font-size:18px;">
  <tr>
  <th style="text-align:center;">ลำดับ</th>
  <th style="text-align:center;">ชื่อผู้ใช้</th>
  <th style="text-align:center;">รหัสผ่าน</th>
  <th style="text-align:center;">สิทธิ์ผู้ใช้</th>
  <th style="text-align:center;">แก้ไข</th>
  <th style="text-align:center;">ลบ</th>
</tr>
<?php
$i = 1;
$permission = "";
require('connect.php');
$stmt = $con->query("SELECT * FROM member");
while($row = $stmt->fetch())
{
  $mem_id = $row['mem_id'];
  if($row['permission'] == 'admin')
  {
    $permission = "ผู้ดูแล";
  }
  else
  {
    $permission = "สมาชิก";
  }
  echo "<tr>
  <td>".$i."</td>
  <td>".$row['username']."</td>
  <td>".str_repeat ('•', strlen ($row['password']))."</td>
  <td>".$permission."</td>
  <td><a href = 'updateMember_view.php?mem_id=$mem_id'>แก้ไข<a></td>
  <td><a href = 'deleteMember_view.php?mem_id=$mem_id'>ลบ<a></td>
  </tr>";
  $i++;
}
    ?>
  </table>
    </body>
</html>
