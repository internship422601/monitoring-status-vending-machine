<?php
function diff2time($time_a){
  date_default_timezone_set("Asia/Bangkok");
    $time_diff=abs($time_a);
    $time_diff_h=floor($time_diff/3600); // จำนวนชั่วโมงที่ต่างกัน
    $time_diff_m=floor(($time_diff%3600)/60); // จำวนวนนาทีที่ต่างกัน
    $time_diff_s=($time_diff%3600)%60; // จำนวนวินาทีที่ต่างกัน
    return $time_diff_h." ชั่วโมง ".$time_diff_m." นาที ".$time_diff_s." วินาที ";
}
function DateTimeDiff($strDateTime1,$strDateTime2)
{
    return (strtotime($strDateTime2) - strtotime($strDateTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}
function table($stmt,$rowcount){
  echo "<p id='Kanit' align = 'center' style='font-size:20px;'>จำนวนทั้งหมด :&nbsp;".$rowcount."</p>";
  echo "<p id='Kanit' align = 'center' style='font-size:20px;color:red;'>*หมายเหตุ จำนวนเวลาที่ใช้จริงเป็นสีแดงเนื่องจากเกิน 30 ชั่วโมงแสดงว่าเครื่องทำงานล่าช้า</p>";
  echo "<table class=\"table\" border=\"2\" id='Kanit' align = 'center' style=\"text-align:center;width:80%;background-color:white;font-size:18px;\">
  <tr>
  <th style=\"text-align:center;\">ลำดับ</th>
  <th style=\"text-align:center;\">ช่องที่</th>
  <th style=\"text-align:center;\">รหัสตู้</th>
  <th style=\"text-align:center;\">ประเภทตู้</th>
  <th style=\"text-align:center;\">วัน/เวลา เข้า</th>
  <th style=\"text-align:center;\">วัน/เวลา ออก</th>
  <th style=\"text-align:center;\">จำนวนเวลาที่ใช้จริง</th>
</tr>";
$i = 1;
$j = 1;
$error = "";
$_month_name = array("01"=>"มกราคม",  "02"=>"กุมภาพันธ์",  "03"=>"มีนาคม",
"04"=>"เมษายน",  "05"=>"พฤษภาคม",  "06"=>"มิถุนายน",
"07"=>"กรกฎาคม",  "08"=>"สิงหาคม",  "09"=>"กันยายน",
"10"=>"ตุลาคม", "11"=>"พฤศจิกายน",  "12"=>"ธันวาคม");
    while($row = $stmt->fetch())
      {
        $datein=date($row['datetime_in']);
        $dateout=date($row['datetime_out']);
        $dates =  strtotime($datein);
        $y1 = date('Y',$dates);
        $m1 = date('m',$dates);
        $d1 = date('d',$dates);
        $h1 = date('H',$dates);
        $i1 = date('i',$dates);
        $s1 = date('s',$dates);
        $dates2 =  strtotime($dateout);
        $y2 = date('Y',$dates2);
        $m2 = date('m',$dates2);
        $d2 = date('d',$dates2);
        $h2 = date('H',$dates2);
        $i2 = date('i',$dates2);
        $s2 = date('s',$dates2);
        $yy1 = $y1+543;
        $yy2 = $y2+543;
        if ($d1<10){
        $d1=substr($d1,1,2);
        }
        if ($d2<10){
        $d2=substr($d2,1,2);
        }
        $locker = $row['lock'].$row['number'];
        if(($row['time_diffs']/(3600)) > 30)
        {
          $error = "<span style='color:red;'>".diff2time($row['time_diffs'])."</span>";
        }
        else {
          $error = diff2time($row['time_diffs']);
        }
        echo "<tr>
        <td>".$i."</td>
        <td>".$locker."</td>
        <td>".$row['id']."</td>
        <td>".$row['id_type']."</td>
        <td>".$d1 ." ".$_month_name[$m1]."  ".$yy1."&nbsp;/&nbsp;".$h1.":".$i1."&nbsp;น.</td>
        <td>".$d2 ." ".$_month_name[$m2]."  ".$yy2."&nbsp;/&nbsp;".$h2.":".$i2."&nbsp;น.</td>
        <td>".$error."</td>
        </tr>";
        $i++;
      }
  echo "</table>";
}
function Login($username,$password)
{
    session_start();
    require('connect.php');
    $stmt=$con->query("SELECT * FROM member WHERE username = '$username' AND password = '$password'");
    $row = $stmt->fetch();
    if($username == $row['username'] && $password == $row['password'])
        {
            $_SESSION['mem_id'] = $row['mem_id'];
            $_SESSION['username'] = $username;
            $_SESSION['password'] = $password;
            $_SESSION['permission'] = $row['permission'];
            header('Location: index.php');
        }
    else
        {
            echo "<center><span style=\"color:red;\" id='Kanit'>"."invalid user</span></center>";
        }
}

 ?>
