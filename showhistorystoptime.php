<?php
session_start();
if(empty($_SESSION['username']) && empty($_SESSION['password']))
{
  header('Location: login.php');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <script>
  function chkNumber(ele)
  {
  var vchar = String.fromCharCode(event.keyCode);
  if ((vchar<'0' || vchar>'9') && (vchar != '.'))
  return false;
  ele.onKeyPress=vchar;
  }
  </script>
  <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
  <style>
  #Kanit{
    font-family: 'Kanit', sans-serif;
  }
  #center{
    text-align: center;
  }
  .table tr:hover {background-color: #ddd;}
  .table th {
  padding-top: 12px;
  padding-bottom: 12px;
  background-color:#CC0000;
  color: white;
  }
  </style>
  <title>ประวัติการกดหยุด/เริ่มเวลา</title>
  <link rel="shortcut icon" href="photo/main-logo.png" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"></head>
  <body style="background-color:WhiteSmoke;"><br>
  <h1 align = 'center' id='Kanit' ><u><b>ประวัติการกดหยุด/เริ่มเวลา</b></u></h1><br><br>
  <p align = 'center'><input type="button" class = 'btn btn-danger' id='Kanit' value="ย้อนกลับ"  onclick="window.location.href = 'index.php'"/></p>

<?php
include('time_function.php');
require('connect.php');
echo "</br><table class=\"table\" border=\"2\" id='Kanit' align = 'center' style=\"text-align:center;width:60%;background-color:white;font-size:16px;\">
<tr>
<th style=\"text-align:center;\">ลำดับ</th>
<th style=\"text-align:center;\">ช่อง</th>
<th style=\"text-align:center;\">รหัสตู้</th>
<th style=\"text-align:center;\">ประเภท</th>
<th style=\"text-align:center;\">สถานะการกด</th>
<th style=\"text-align:center;\">วันที่/เวลา</th>
</tr>";
$i = 1;
$status = "";
$_month_name = array("01"=>"มกราคม",  "02"=>"กุมภาพันธ์",  "03"=>"มีนาคม",
"04"=>"เมษายน",  "05"=>"พฤษภาคม",  "06"=>"มิถุนายน",
"07"=>"กรกฎาคม",  "08"=>"สิงหาคม",  "09"=>"กันยายน",
"10"=>"ตุลาคม", "11"=>"พฤศจิกายน",  "12"=>"ธันวาคม");
$stmt = $con->query("SELECT * FROM historystoptime");
while($row = $stmt->fetch())
{
  if($row['status'] == 1)
  {
    $status = "<span style='color:red;'>หยุด</span>";
  }
  else if($row['status'] == 0)
  {
    $status = "<span style='color:blue;'>เริ่ม</span>";
  }
  $date = $row['time_stamp'];
  $dates =  strtotime($date);
  $y1 = date('Y',$dates);
  $m1 = date('m',$dates);
  $d1 = date('d',$dates);
  $h1 = date('H',$dates);
  $i1 = date('i',$dates);
  $s1 = date('s',$dates);
  $yy1 = $y1 +543;
  if ($d1<10){
  $d1=substr($d1,1,2);
  }
  echo "<tr><td>".$i."</td>
  <td>".$row['lock'].$row['number']."</td>
  <td>".$row['id']."</td>
  <td>".$row['id_type']."</td>
  <td>".$status."</td>
  <td>".$d1 ." ".$_month_name[$m1]."  ".$yy1."&nbsp;/&nbsp;".$h1.":".$i1."&nbsp;น.</td>
  </tr>";
  $i++;
}
?>
