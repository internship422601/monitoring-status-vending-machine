<?php
session_start();
if(empty($_SESSION['username']) && empty($_SESSION['password']))
{
  header('Location: login.php');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
  <style>
  #Kanit{
    font-family: 'Kanit', sans-serif;
  }
  #center{
    text-align: center;
  }
  .table tr:hover {background-color: #ddd;}
  .table th {
  padding-top: 12px;
  padding-bottom: 12px;
  background-color:#CC0000;
  color: white;
  }
  </style>
  <title>แก้ไขผู้ใช้</title>
  <link rel="shortcut icon" href="photo/main-logo.png" />
  <br><br><div id="center">
    <img src="photo/main-logo.png" alt="Sun108" width="270" height="250"></div>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"></head>

  <body style="background-color:WhiteSmoke;"><br>
  <h2 align = 'center' id='Kanit' style="font-size:55px;margin-right:2.9%;"><u><b>แก้ไขผู้ใช้</b></u></h2><br><br>
  <?php
  $mem_id = $_GET['mem_id'];
  require('connect.php');
  $stmt = $con->query("SELECT * FROM member WHERE mem_id = '$mem_id'");
  $row = $stmt->fetch();
   ?>

  <form method="post" action="updateMember.php">
  <center>
      <span id='Kanit' style="font-size : 30px;margin-left:2.5%;"><b>Username : </b></span>
      <input type="text" style="font-size:20px"  maxlength="20" id='Kanit' name="username" value="<?= $row['username']; ?>" readonly/><br><br>

  <span id='Kanit' style="font-size : 30px;margin-left:3%;"><b>Password : </b></span>
      <input type="password" style="font-size:20px"  maxlength="20" id='Kanit' name="password" value="<?= $row['password']; ?>"/><br><br>

<span style="margin-right:3.5%;">
  <span id='Kanit' style="font-size : 30px;"><b>Permission : </b></span>
      <select name ="permission" id='Kanit' style="font-size:26px;">


    <?php
    if($row['permission'] == 'admin'){
      echo "<option value=\"admin\" style=\"font-size:20px\"selected>ผู้ดูแล</option>
      <option value=\"user\" style=\"font-size:20px\">สมาชิก</option>";
    }
    else if($row['permission'] == 'user'){
      echo "<option value=\"admin\" style=\"font-size:20px\">ผู้ดูแล</option>
      <option value=\"user\" style=\"font-size:20px\" selected>สมาชิก</option>";
    }
    ?>

    </select></span>
      <br><br><br><br>
  <input type="hidden" name="mem_id" value="<?=$mem_id?>"/>
  <button type="submit" class="btn btn-warning" id="Kanit" style="text-align:center; height: 50px; width: 12   0px; font-size: 27px;">แก้ไข</button>
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button type="button" class="btn btn-default" id="Kanit"
            onclick="window.location.href='member_management.php';" style="text-align:center; height: 50px; width: 150px; font-size: 27px; background-color: lightgrey">ย้อนกลับ</button>
      </center></form>
