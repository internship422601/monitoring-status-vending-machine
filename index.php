<?php
session_start();
if(empty($_SESSION['username']) && empty($_SESSION['password']))
{
  header('Location: login.php');
}
 ?>
<html>

    <head>
        <title>status</title>
        <link rel="stylesheet" type="text/css" href="style_index.css">
        <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
        <link rel="shortcut icon" href="photo/main-logo.png" />
        <meta charset="utf-8">
        <meta name="expires" content="never" />
        <br><div style="margin-left: 2%;">
        <table><tr>
        <td><img id="logo" src="photo/main-logo.png"></td>
        <td style="vertical-align:top;"><h1>&nbsp;&nbsp;Status Vending Machine</h1></td>
      </tr></table></div>
        <div class="clock" ><h1 id="currentTime"></h1></div>


        <div style="margin-top:-5%;margin-left:14.75%;">
          <?php
          if($_SESSION['permission'] == 'admin')
          {
            echo
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='history' onclick=\"window.location.href='showhistory.php'\">ประวัติ</button>

            <div class=\"dropdown\" style=\"margin-left:0.45%;\">
            <button onclick=\"myFunction()\" class=\"dropbtn\">การจัดการระบบ</button>
            <div id=\"myDropdown\" class=\"dropdown-content\">

            <a class='' href=\"member_management.php\">การจัดการผู้ใช้</a>

            <a class='starttime' href=\"timestartall.php\">เริ่มเวลาทั้งไลน์ผลิต</a>

            <a class='stoptime' href=\"timestopall.php\">หยุดเวลาทั้งไลน์ผลิต</a>


            </div></div>
            &nbsp;

            <button class='logout' onclick=\"window.location.href='logout.php'\" style=\"color:black;\">ออกจากระบบ</button>";
      }
            else{
                echo "<div style=\"margin-top: -5%;margin-left:2%;\">
            <button class='history' onclick=\"window.location.href='showhistory.php'\">ประวัติ</button>
            &nbsp;
            <button class='logout' onclick=\"window.location.href='logout.php'\" style=\"color:black;\">ออกจากระบบ</button>
            </div>"


                    ;}
        ?>
                    <script>function myFunction() {
                document.getElementById("myDropdown").classList.toggle("show");}
                window.onclick = function(event) {
                if (!event.target.matches('.dropbtn')) {
                    var dropdowns = document.getElementsByClassName("dropdown-content");
                    var i;
                    for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
      }
    }
  }
}
        </script>
        </div>
    </head>

    <body style="">

        <script>
        window.onload = function() {
  clock();
    function clock() {
    var now = new Date();
    var TwentyFourHour = now.getHours();
    var hour = now.getHours();
    var min = now.getMinutes();
    var sec = now.getSeconds();
    if (min < 10) {
      min = "0" + min;
    }
    if (sec < 10) {
      sec = "0" + sec;
    }
    if(hour < 10)
        {
           hour  = "0" + hour;
        }



  document.getElementById('currentTime').innerHTML =     hour+':'+min+':'+sec;
    setTimeout(clock, 1000);
    }
}
        </script>
        <br>
        <div class="col">
        <table class="tb"><tr>

            <td><input type="button" class="btnred"></td>
            <td><span id="btnred">&nbsp; เกิน 30 ชั่วโมง &nbsp;&nbsp;</span></td>

            <td><input type="button" class="btnorange"></td>
            <td><span id="btnorange">&nbsp; 24 - 30 ชั่วโมง &nbsp;&nbsp;</span></td>

            <td><input type="button" class="btngreen"></td>
            <td><span id="btngreen">&nbsp; 0 - 24 ชั่วโมง &nbsp;&nbsp;</span></td>

            <td><input type="button" class="btnwhite"></td>
            <td><span id="btnwhite">&nbsp; ว่าง &nbsp;&nbsp;</span></td>

        </tr></table>
        </div>

    <br><br>
        <?php
        header("Refresh:30");
        date_default_timezone_set("Asia/Bangkok");
        include('time_function.php');
        require('connect.php');
        $stmt = $con->query("SELECT * FROM locker INNER JOIN typelocker WHERE locker.id_type = typelocker.id_type  ORDER BY locker.lock ASC ,number DESC ");
        $color = "";
        $link = "";
        $p = 1;
        $x = 1;
        echo "<div class=\"row\">";
        while($row = $stmt->fetch())
    {
        $locker = $row['lock'];
        $number = $row['number'];
        $No = $row['No'];
        $picture = $row['picture'];
        $lock = $locker.$number;
          if(isset($row['datetimes']))
          {
          if(empty($row['save_hour']))
          {
            date_default_timezone_set("Asia/Bangkok");
            $hours = DateTimeDiff($row['datetimes'],date("Y-m-d H:i:s"));
             // echo $hours;
          }
          else if(isset($row['save_hour']))
          {
              if(isset($row['datetime_stop']))
              {
                $hours = $row['save_hour'];
                 // echo $hours;
              }
              else
              {
                date_default_timezone_set("Asia/Bangkok");
                $hour = DateTimeDiff($row['datetime_start'],date("Y-m-d H:i:s"));
                $hours = $row['save_hour'] + $hour;
                 // echo $hours;
            }
          }
          if($hours>=24 && $hours <30)//24-30ชม.
          {
            if($row['status'] == 1)
            {
            $updatestatus = "UPDATE locker SET status = '2'  WHERE locker.No = '$No'";
            $con->exec($updatestatus);
            }
          }
          else if ($hours>=30)//มากกว่า 30 ชม.
          {
            if($row['status'] == 2 || $row['status'] == 1)
            {
            $updatestatus2 = "UPDATE locker SET status = '3'  WHERE locker.No = '$No'";
            $con->exec($updatestatus2);
            }
          }
        }
        if(isset($row['status']) && $row['status_time'] == 0)
        {
          $status = $row['status'];
          if($status == 1)
          {
            $color = "limegreen";
            $link = "window.location.href = 'detail_locker.php?lock=$locker&number=$number'";

          }
          else if($status == 2)
          {
            $color = "orange";
            $link = "window.location.href = 'detail_locker.php?lock=$locker&number=$number'";
          }
          else if($status == 3)
          {
            $color = "red";
            $link ="window.location.href = 'detail_locker.php?lock=$locker&number=$number'";
          }
          else if($status == 0){
            $color = "white";
            $link = "window.location.href = 'addlocker.php?lock=$locker&number=$number'";
          }
        }
        else if (isset($row['status']) && $row['status_time'] == 1)
        {
          $color = "gray";
          $link = "window.location.href = 'detail_locker.php?lock=$locker&number=$number'";
        }
        if($p >= 1 && $p <= 4)
        {
            if($x == 1)
            {
              echo "<div class=\"column\">
              <input type=\"hidden\" class=\"btn\" value=\"\"  style = 'background-color: black;'/>
              <img hre src=\"photo/black.png\"><br><br>
              <input type=\"button\" class=\"btn\" value=\"$lock\"  style = 'background-color: $color;' onclick=\"$link\"/>
              <img hre src=\"photo/$picture\"><br><br>";
              $x++;
            }
            else if($x == 10)
            {
              echo  "<div class=\"column\">";
              if($p == 4)
              {
                echo "<input type=\"button\" class=\"btn\" value=\"$lock\" style = 'background-color: $color;' onclick=\"$link\"/>
                <img hre src=\"photo/$picture\"><br><br>";
              }
              else{
              echo "<input type=\"hidden\" class=\"btn\" value=\"\"  style = 'background-color: black;' />
              <img hre src=\"photo/black.png\"><br><br>
              <input type=\"button\" class=\"btn\" value=\"$lock\" style = 'background-color: $color;' onclick=\"$link\"/>
              <img hre src=\"photo/$picture\"><br><br>";
            }
              $x = 2;
              $p++;
            }
            else if ($x == 9)
            {
              echo  "<input type=\"button\" class=\"btn\" value=\"$lock\" style = 'background-color:$color;' onclick=\"$link\"/>
              <img hre src=\"photo/$picture\"><br><br>";
              echo "</div>";
              $x++;
            }
            else {
                echo  "<input type=\"button\" class=\"btn\" value=\"$lock\" style = 'background-color: $color;' onclick=\"$link\"/>
                <img hre src=\"photo/$picture\"><br><br>
                ";
                $x++;
              }
        }
        else if ($p >= 5)
        {
          if($x == 1)
          {
            echo "<div class=\"column\">
            <input type=\"button\" class=\"btn\" value=\"$lock\"  style = 'background-color: $color;' onclick=\"$link\">
            <img hre src=\"photo/$picture\"><br><br>";
            $x++;
          }
          else if($x == 11)
          {
            echo  "<div class=\"column\">
            <input type=\"button\" class=\"btn\" value=\"$lock\" style = 'background-color: $color;' onclick=\"$link\">
            <img hre src=\"photo/$picture\"><br><br>";
            $x = 2;
            $p++;
          }
          else if ($x == 10)
          {
            echo  "<input type=\"button\" class=\"btn\" value=\"$lock\" style = 'background-color:$color;' onclick=\"$link\">
            <img hre src=\"photo/$picture\"><br><br>";
            echo "</div>";
            $x++;
          }
          else {
              echo  "<input type=\"button\" class=\"btn\" value=\"$lock\" style = 'background-color: $color;' onclick=\"$link\">
              <img hre src=\"photo/$picture\"><br><br>";
              $x++;
            }
        }
      }
    echo "</div>";
?>

    </body>
</html>
