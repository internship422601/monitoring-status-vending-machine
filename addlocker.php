<?php
session_start();
if(empty($_SESSION['username']) && empty($_SESSION['password']))
{
  header('Location: login.php');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<?php
require('connect.php');
date_default_timezone_set("Asia/Bangkok");
$lock = $_GET['lock'];
$number = $_GET['number'];
?>
<head>
  <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
  <style>

html{
    width: 1920px;
    height: 1080px;
}
  #Kanit{
      font-family: 'Kanit', sans-serif;
      }
      .open-button {
      background-color: #39ac73;
      color: white;
      padding: 16px 20px;
      border: none;
      cursor: pointer;
      opacity: 0.8;
      position: fixed;
      bottom: 23px;
      right: 28px;
      width: 280px;
    }
    .open-button1 {
    background-color: #CC0000;
    color: white;
    padding: 16px 20px;
    border: none;
    cursor: pointer;
    opacity: 0.8;
    position: fixed;
    bottom: 23px;
    right: 336px;
    width: 280px;
  }


    /* The popup form - hidden by default */
    .form-popup {
      display: none;
      position: fixed;
      bottom: 0;
      right: 15px;
      border: 3px solid #f1f1f1;
      z-index: 9;
    }
    .form-popup1 {
      display: none;
      position: fixed;
      bottom: 0;
      right: 323px;
      border: 3px solid #f1f1f1;
      z-index: 9;
    }

    /* Add styles to the form container */
    .form-container {
      max-width: 300px;
      padding: 10px;
      background-color: white;
    }

    /* Full-width input fields */
    .form-container input[type=text], .form-container input[type=password] {
      width: 100%;
      padding: 15px;
      margin: 5px 0 22px 0;
      border: none;
      background: #f1f1f1;
    }

    /* When the inputs get focus, do something */
    .form-container input[type=text]:focus, .form-container input[type=password]:focus {
      background-color: #ddd;
      outline: none;
    }

    /* Set a style for the submit/login button */
    .form-container .btn {
      background-color: #39ac73;
      color: white;
      padding: 16px 20px;
      border: none;
      cursor: pointer;
      width: 100%;
      margin-bottom:10px;
      opacity: 0.8;
    }

    /* Add a red background color to the cancel button */
    .form-container .cancel {
      background-color: #CC0000;
    }

    /* Add some hover effects to buttons */
    .form-container .btn:hover, .open-button:hover, .open-button1:hover {
      opacity: 1;
    }
      .btn btn-default:hover{
          background-color: white;
      }


  </style>
  <title>Sun108</title>
  <link rel="shortcut icon" href="photo/main-logo.png" />
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body style="background-color:WhiteSmoke;" >
  <br><br>
  <div style="margin-left:43%;">
<img src="photo/main-logo.png" alt="Sun108" width="270" height="250">
</div>
<div class="container">
  <h2 align = 'center' id='Kanit' style="padding-right:5%;font-size:55px;"><b>กรอกข้อมูลตู้ช่อง <?=$lock.$number?></b></h2><br>
  <form class="form-horizontal" action="addlocker.php?lock=<?= $lock;?>&number=<?= $number; ?>" method="post">
    <div class="form-group">
      <center style="margin-left:10%;"><label class="control-label col-sm-2" id="Kanit" style="font-size : 30px;">รหัสตู้ :</label></center>
        <input type="text" class="form-control" id="Kanit" maxlength="20"  placeholder="Enter id" name="id" style="width:50%;margin-top:1%;" required/>
    </div><br>

      <span><label class="control-label col-sm-2" id="Kanit" style="font-size : 30px; margin-top:-0.25%; margin-left:7.5%;">ประเภทตู้ :</label>
      <div class="col-sm-10" style="margin-left:25%;margin-top:-4%; width: 100%;">
        <?php
        $i =1;
        $stmt = $con->query("SELECT * FROM typelocker");
        while($row = $stmt->fetch()){
          $id_type = $row['id_type'];
          $picture = $row['picture'];
          if($id_type != "0")
          {
          if($i >= 1 && $i <=2){
          echo "<input type=\"radio\" style=\"\"id=\"id_type\" name=\"id_type\" value=\"$id_type\">&nbsp;<x id='Kanit' style='font-size:20px;'>$id_type</x>&nbsp;<img src=\"photo/$picture\" alt=\"Sun108\" width=\"100\" height=\"100\"></input>&nbsp;&nbsp;&nbsp;&nbsp;";
          $i++;
            }
          else if($i == 3){
            echo "<input type=\"radio\" style=\"\"id=\"id_type\" name=\"id_type\" value=\"$id_type\">&nbsp;<x id='Kanit' style='font-size:20px;'>$id_type</x>&nbsp;<img src=\"photo/$picture\" alt=\"Sun108\" width=\"100\" height=\"100\"></input>&nbsp;&nbsp;&nbsp;&nbsp;<br><br>";
            $i = 1;
          }
          }
        }
        ?>
      </div>
    </span>
    <div style="text-align:center;margin-top:25%;">
        <input type="hidden" name="currentdate" value="<?= date("Y-m-d H:i:s");?>"/>
        <button type="submit" class="btn btn-success" id="Kanit" style="height: 50px; width: 100px; font-size: 27px;">เพิ่ม</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="reset" class="btn btn-danger" id="Kanit" style="height: 50px; width: 100px; font-size: 27px;">รีเซ็ต</button>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-default" id="Kanit"
                onclick="window.location.href='index.php';" style="height: 50px; width: 200px; font-size: 27px; background-color: lightgrey">ย้อนกลับ</button>
    </div>
  </form>
</div>
<?php
if($_SESSION['permission'] == 'admin')
 {
   echo "<button class=\"open-button\" id='Kanit' onclick=\"openForm()\" style=\"font-size: 23px;\">เพิ่มประเภทตู้</button>
   <button class=\"open-button1\" id='Kanit' onclick=\"openForm1()\" style=\"font-size: 23px;\">ลบประเภทตู้</button>";
}
?>

<div class="form-popup" id="myForm">
  <form action="upload_addpic.php" class="form-container" method="post" enctype="multipart/form-data">
    <h1 id='Kanit'>เพิ่มประเภทตู้</h1><br>

    <label for="id_type"><b id='Kanit' style="font-size:18px;">ชื่อตู้ :</b></label>
    <input type="text" maxlength="20"  id='Kanit' placeholder="Enter name" name="id_type" required>
    <input type="hidden" name='lock' value="<?php echo $lock; ?>"/>
    <input type="hidden" name='number' value="<?php echo $number; ?>"/>
    <label for="picture"><b id='Kanit' style="font-size:18px;">รูปภาพ :</b><x style = "color:red;font-size:18px;" id='Kanit' >&nbsp;&nbsp;*(จำเป็นต้องใส่รูป)</x></label>
    <input type="file" id='Kanit' name="file" accept="image/*" required><br><br><br>

    <button type="submit" class="btn" id='Kanit' style="font-size: 23px;">เพิ่ม</button>
    <button type="reset" class="btn cancel" onclick="closeForm()" id='Kanit' style="font-size: 23px;">ยกเลิก</button>
  </form>
</div>
<div class="form-popup1" id="myForm1">
  <form action="delete_pic.php" class="form-container" method="post">

    <h1 id='Kanit'>ลบประเภทตู้</h1><br>
    <label for="id_type"><b id='Kanit' style="font-size:18px;">ชื่อตู้ :</b></label>
    <select name='id_type' id = 'Kanit'>
    <?php
      $stmt = $con->query("SELECT * FROM typelocker");
      while($row = $stmt->fetch())
      {
        $id_type = $row['id_type'];
        $picture = $row['picture'];
        if($id_type != "0")
        {
        echo "<option value = \"$id_type\" style=\"font-size:18px;\">&nbsp;$id_type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
        }
      }
     ?>
    </select>
    <input type="hidden" name='lock' value="<?php echo $lock; ?>"/>
    <input type="hidden" name='number' value="<?php echo $number; ?>"/>


    <button type="submit" class="btn" id='Kanit' style="font-size: 23px; margin-top:20%;">ลบ</button>
    <button type="reset" class="btn cancel" onclick="closeForm1()" id='Kanit' style="font-size: 23px;">ยกเลิก</button>
  </form>
</div>

<script>
function openForm() {
  document.getElementById("myForm").style.display = "block";
  document.getElementById("myForm1").style.display = "none";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}
function openForm1() {
  document.getElementById("myForm1").style.display = "block";
  document.getElementById("myForm").style.display = "none";
}

function closeForm1() {
  document.getElementById("myForm1").style.display = "none";
}
function closeFormall() {
  document.getElementById("myDiv").style.display = "none";
}
</script>
</body>
</html>
<?php
if(isset($_POST['id']) && isset($_POST['currentdate']) && isset($_POST['id_type']) )
{
    date_default_timezone_set("Asia/Bangkok");
    $id = $_POST['id'];
    $currentdate = $_POST['currentdate'];
    $id_type = $_POST['id_type'];
    $stmt = $con->query("SELECT * FROM locker WHERE locker.lock = '$lock' AND locker.number = $number");
    $row = $stmt->fetch();
    if($row['id'] != NULL)
    {
      echo "</br><center><p id ='Kanit' style='font-size:18px;color:red;'>ช่อง $lock$number มีการใช้งานอยู่แล้ว</p><center>";
    }
    else {
      $updatelock = "UPDATE locker SET id = '$id' ,datetimes = '$currentdate',status = '1',id_type = '$id_type' , datetime_start = '$currentdate',status_time = 0 WHERE locker.lock = '$lock' AND locker.number = $number";
      $success = $con->exec($updatelock);
      if ($success === false)
      {
          die(print_r($con->errorInfo(), true));
      }
      else
      {
          $insertstatistics = "INSERT INTO `statistics` (`No`, `lock`, `number`, `id`, `id_type`, `datetime_in`, `datetime_out`,`time_diffs`) VALUES (NULL, '$lock', '$number', '$id', '$id_type', '$currentdate', NULL,NULL)";
          $success = $con->exec($insertstatistics);
          if ($success === false)
          {
              die(print_r($con->errorInfo(), true));
          }
          else
          {
          echo "<script>location.href ='index.php';</script>";
          }
      }
  }
}
?>
