-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2019 at 06:29 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sun108`
--

-- --------------------------------------------------------

--
-- Table structure for table `locker`
--

CREATE TABLE `locker` (
  `No` int(100) NOT NULL,
  `lock` varchar(1) CHARACTER SET utf8 NOT NULL,
  `number` int(3) NOT NULL,
  `id` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `datetimes` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `id_type` varchar(100) CHARACTER SET utf8 DEFAULT '0',
  `datetime_start` datetime DEFAULT NULL,
  `datetime_stop` datetime DEFAULT NULL,
  `save_hour` double DEFAULT NULL,
  `time_diff` int(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `locker`
--

INSERT INTO `locker` (`No`, `lock`, `number`, `id`, `datetimes`, `status`, `id_type`, `datetime_start`, `datetime_stop`, `save_hour`, `time_diff`) VALUES
(0, 'D', 5, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(1, 'A', 1, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(2, 'A', 2, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(3, 'A', 3, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(4, 'A', 4, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(5, 'A', 5, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(6, 'A', 6, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(7, 'A', 7, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(8, 'A', 8, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(9, 'A', 9, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(10, 'B', 1, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(11, 'B', 2, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(12, 'B', 3, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(13, 'B', 4, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(14, 'B', 5, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(15, 'B', 6, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(16, 'B', 7, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(17, 'B', 8, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(18, 'B', 9, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(19, 'C', 1, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(20, 'C', 2, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(21, 'C', 3, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(22, 'C', 4, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(23, 'C', 5, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(24, 'C', 6, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(25, 'C', 7, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(26, 'C', 8, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(27, 'C', 9, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(28, 'D', 1, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(29, 'D', 2, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(30, 'D', 3, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(31, 'D', 4, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(33, 'D', 6, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(34, 'D', 7, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(35, 'D', 8, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(36, 'D', 9, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(37, 'E', 1, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(38, 'E', 2, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(39, 'E', 3, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(40, 'E', 4, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(41, 'E', 5, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(42, 'E', 6, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(43, 'E', 7, '5645644sfda', '2019-06-20 17:09:47', 3, 'FAA', '2019-06-20 17:09:47', NULL, NULL, 0),
(44, 'E', 8, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(45, 'E', 9, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(46, 'E', 10, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(47, 'F', 1, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(48, 'F', 2, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(49, 'F', 3, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(50, 'F', 4, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(51, 'F', 5, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(52, 'F', 6, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(53, 'F', 7, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(54, 'F', 8, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(55, 'F', 9, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(56, 'F', 10, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(57, 'G', 1, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(58, 'G', 2, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(59, 'G', 3, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(60, 'G', 4, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(61, 'G', 5, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(62, 'G', 6, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(63, 'G', 7, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(64, 'G', 8, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(65, 'G', 9, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(66, 'G', 10, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(67, 'H', 1, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(68, 'H', 2, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(69, 'H', 3, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(70, 'H', 4, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(71, 'H', 5, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(72, 'H', 6, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(73, 'H', 7, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(74, 'H', 8, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(75, 'H', 9, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(76, 'H', 10, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(77, 'I', 1, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(78, 'I', 2, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(79, 'I', 3, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(80, 'I', 4, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(81, 'I', 5, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(82, 'I', 6, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(83, 'I', 7, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(84, 'I', 8, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(85, 'I', 9, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(86, 'I', 10, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(87, 'J', 1, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(88, 'J', 2, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(89, 'J', 3, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(90, 'J', 4, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(91, 'J', 5, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(92, 'J', 6, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(93, 'J', 7, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(94, 'J', 8, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(95, 'J', 9, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(96, 'J', 10, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(97, 'K', 1, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(98, 'K', 2, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(99, 'K', 3, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(100, 'K', 4, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(101, 'K', 5, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(102, 'K', 6, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(103, 'K', 7, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(104, 'K', 8, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(105, 'K', 9, NULL, NULL, 0, '0', NULL, NULL, NULL, 0),
(106, 'K', 10, NULL, NULL, 0, '0', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `mem_id` int(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `permission` enum('admin','user') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`mem_id`, `username`, `password`, `permission`) VALUES
(1, 'admin', 'admin', 'admin'),
(2, 'tuyu111', '034206822', 'user'),
(3, 'tuyu222', '034206822', 'user'),
(5, 'tuyu555', '034206822', 'user'),
(6, 'tuyu777', 'tuyu777', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `statistics`
--

CREATE TABLE `statistics` (
  `No` int(255) NOT NULL,
  `lock` varchar(20) CHARACTER SET utf8 NOT NULL,
  `number` int(3) NOT NULL,
  `id` varchar(30) CHARACTER SET utf8 NOT NULL,
  `id_type` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `datetime_in` datetime NOT NULL,
  `datetime_out` datetime DEFAULT NULL,
  `time_diffs` int(255) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `statistics`
--

INSERT INTO `statistics` (`No`, `lock`, `number`, `id`, `id_type`, `datetime_in`, `datetime_out`, `time_diffs`) VALUES
(41, 'E', 9, 'ad4888', 'MAMA', '2019-06-18 13:05:52', '2019-06-18 13:06:36', 41),
(42, 'E', 7, 'asdasdasd', 'MAMA', '2019-06-18 13:10:23', '2019-06-18 13:20:58', 256),
(43, 'F', 6, 'asdasdw', 'Coffee', '2019-06-18 13:16:50', '2019-06-18 13:23:27', 236),
(44, 'F', 10, '654654', '6G', '2019-06-18 13:25:39', '2019-06-18 13:27:57', 136),
(45, 'F', 7, '65465468', 'Cup', '2019-06-18 13:28:17', '2019-06-18 13:36:36', 217),
(46, 'F', 8, 'sadasdsa', '6G', '2019-06-18 13:36:52', '2019-06-18 14:03:55', 1616),
(47, 'F', 7, 'dfdsfsd', 'Can', '2019-06-18 13:42:55', '2019-06-18 15:01:15', 4634),
(48, 'E', 7, 'sdfsdf', 'Can', '2019-06-18 13:53:19', '2019-06-18 15:01:13', 4012),
(49, 'E', 10, 'ปแดกหด', 'Can', '2019-06-18 15:16:18', '2019-06-18 15:32:03', 943),
(50, 'E', 10, 'ฟหกฟหก', '6G', '2019-06-18 15:32:04', '2019-06-18 15:39:06', 201),
(51, 'E', 9, 'หกหฟกฟหก', '6G', '2019-06-18 15:39:26', '2019-06-18 15:39:44', 9),
(52, 'E', 10, 'ฟหกฟหก', 'Can', '2019-06-19 14:06:37', '2019-06-19 14:07:21', 23),
(53, 'E', 10, '564564หฟก', '6G', '2019-06-19 14:07:22', '2019-06-19 14:34:01', 300),
(54, 'E', 10, 'ฟหกหฟก', '6G', '2019-06-19 14:34:02', '2019-06-19 16:26:20', 185),
(55, 'F', 10, 'ฟกฟหกฟ', 'Can', '2019-06-19 14:46:41', '2019-06-19 16:26:22', 5980),
(56, 'G', 6, 'ฟหกฟหก', 'MAMA', '2019-06-19 14:46:46', '2019-06-19 16:26:30', 5983),
(57, 'F', 5, '59987กหฟ', 'MAMA', '2019-06-19 14:46:52', '2019-06-19 16:26:28', 5975),
(58, 'D', 5, 'ฟปกฟหก', 'MAMA', '2019-06-19 14:46:58', '2019-06-19 16:26:27', 5967),
(59, 'D', 8, 'กไหดกฟหก', '6G', '2019-06-19 14:50:37', '2019-06-19 16:26:24', 181),
(61, 'E', 10, 'asdasdasd', 'Coffee', '2019-06-19 16:31:15', '2019-06-19 16:42:35', 107),
(62, 'E', 7, 'asdadasd', 'FAA\r\n', '2019-06-19 16:31:39', '2019-06-19 16:42:32', 542),
(63, 'F', 6, 'asdasdsa', 'MAMA', '2019-06-19 16:31:43', '2019-06-19 16:42:37', 544),
(64, 'D', 4, 'asdasdsa', 'Cup', '2019-06-19 16:31:46', '2019-06-19 16:42:38', 542),
(65, 'E', 8, 'asdasd', '6G', '2019-06-19 16:39:20', '2019-06-19 16:42:34', 102),
(66, 'E', 10, 'sadsadw', '6G', '2019-06-19 16:42:42', '2019-06-20 09:01:15', 259),
(67, 'E', 8, 'asdasd', 'Cup', '2019-06-19 16:42:50', '2019-06-20 09:01:17', 293),
(68, 'F', 7, 'asdasd', 'FAA\r\n', '2019-06-19 16:42:55', '2019-06-20 09:01:18', 249),
(69, 'E', 10, 'asdasd', 'Can', '2019-06-20 13:08:06', '2019-06-21 11:02:59', 0),
(70, 'E', 9, 'asdasd', 'FAA\r\n', '2019-06-20 13:08:54', '2019-06-21 11:06:22', 78771),
(71, 'F', 8, 'asdasd', 'MAMA', '2019-06-20 13:09:05', '2019-06-21 11:06:47', 78805),
(72, 'E', 7, '5645644sfda', 'FAA\r\n', '2019-06-20 17:09:47', NULL, NULL),
(73, 'E', 8, 'asdasdasd', 'asdasdwa', '2019-06-20 17:17:42', '2019-06-20 17:18:18', 0);

-- --------------------------------------------------------

--
-- Table structure for table `typelocker`
--

CREATE TABLE `typelocker` (
  `id_type` varchar(10) CHARACTER SET utf8 NOT NULL,
  `picture` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `typelocker`
--

INSERT INTO `typelocker` (`id_type`, `picture`) VALUES
('0', 'empty.png'),
('6G', '6GG.png'),
('Can', 'can&bottle.png'),
('Coffee', 'Coffee-1.png'),
('Cup', 'hot&coll.png'),
('FAA', 'new-can.png'),
('MAMA', 'mama-1.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `locker`
--
ALTER TABLE `locker`
  ADD PRIMARY KEY (`No`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`mem_id`);

--
-- Indexes for table `statistics`
--
ALTER TABLE `statistics`
  ADD PRIMARY KEY (`No`);

--
-- Indexes for table `typelocker`
--
ALTER TABLE `typelocker`
  ADD PRIMARY KEY (`id_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `locker`
--
ALTER TABLE `locker`
  MODIFY `No` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `mem_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `statistics`
--
ALTER TABLE `statistics`
  MODIFY `No` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
