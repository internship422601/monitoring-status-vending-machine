<?php
session_start();
if(empty($_SESSION['username']) && empty($_SESSION['password']))
{
  header('Location: login.php');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <script>
  function chkNumber(ele)
  {
  var vchar = String.fromCharCode(event.keyCode);
  if ((vchar<'0' || vchar>'9') && (vchar != '.'))
  return false;
  ele.onKeyPress=vchar;
  }
  </script>
  <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
  <style>
  #Kanit{
    font-family: 'Kanit', sans-serif;
  }
  #center{
    text-align: center;
  }
  .table tr:hover {background-color: #ddd;}
  .table th {
  padding-top: 12px;
  padding-bottom: 12px;
  background-color:#CC0000;
  color: white;
  }
  </style>
  <title>สถิติรายปี</title>
  <link rel="shortcut icon" href="photo/main-logo.png" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"></head>
  <body style="background-color:WhiteSmoke;"><br>
  <h1 align = 'center' id='Kanit' ><u><b>สถิติรายปี</b></u></h1><br><br>
  <form action = 'showhistoryperyear.php' method="post">
  <p id='Kanit' align = 'center' style="font-size:20px;">
    ใส่ปีที่ พ.ศ. จะค้นหา :&nbsp;<input type="text" name="year" maxlength="4" size="4" placeholder="2XXX" OnKeyPress="return chkNumber(this)" required>
    <input type="submit" id='Kanit' class="btn btn-defult" value="ค้นหา"/>
    <input type="button" class = 'btn btn-danger' id='Kanit' value="ย้อนกลับ"  onclick="window.location.href = 'index.php'"/></p>
  </form><center>
  <center><div id="chartContainer" style="height: 450px; width: 60%;"></div></center>
<?php
include('time_function.php');
require('connect.php');
if(isset($_POST['year']))
{
  $y = $_POST['year'];
  $year = $y-543;
  $dataPoints = array();
  $x = 0;
  $stmts = $con->query("SELECT id_type,COUNT(id_type) AS number,SUM(time_diffs) AS total,(SUM(time_diffs)/COUNT(id_type)) AS avg_time FROM `statistics`WHERE Year(datetime_out) = '$year' GROUP BY id_type");
  echo "</br><table class=\"table\" border=\"2\" id='Kanit' align = 'center' style=\"text-align:center;width:40%;background-color:white;font-size:16px;\">
  <tr>
  <th style=\"text-align:center;\">ชื่อตู้</th>
  <th style=\"text-align:center;\">เวลาที่ใช้จริงทั้งหมด(ทุกเครื่อง)</th>
  <th style=\"text-align:center;\">เวลาเฉลี่ยในการทดสอบ/เครื่อง</th>
  <th style=\"text-align:center;\">จำนวน(เครื่อง)</th>
  </tr>";
  while($row = $stmts->fetch())
  {
  $id_type = $row['id_type'];
  $rowcount = $row['number'];
  $avg_time = $row['avg_time'];
  $total = $row['total'];
  $x +=  $rowcount;
  echo "<tr>
  <td>".$id_type."</td>
  <td>".diff2time($total)."</td>
  <td>".diff2time($avg_time)."</td>
  <td>".$rowcount."</td></tr>";
  // table($stmt,$rowcount);
  $dataPoints[] = array("label"=> $id_type , "y"=> $rowcount);
  }
  echo "<tr><td colspan=\"3\"><u><b>รวม</u></b></td><td colspan=\"1\"><b>".$x."</b></td></tr>
  </table>";
}
 ?>

</center>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	exportEnabled: true,
	theme: "light1", // "light1", "light2", "dark1", "dark2"
	title:{
		text: <?php echo json_encode("สถิติ"." "."ปี พ.ศ. ".$y); ?>
	},
	data: [{
		type: "column", //change type to bar, line, area, pie, etc
		//indexLabel: "{y}", //Shows y value on all Data Points
		indexLabelFontColor: "#5A5757",
		indexLabelPlacement: "outside",
		dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
	}]
});
chart.render();
}
</script>
</body>
