<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="login_style.css">
    <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="photo/main-logo.png" />
    <style>
    #Kanit{
      font-family: 'Kanit', sans-serif;
    }
  </style>
</head>

<body>


<div class="div">
    <img id="logo" src="photo/main-logo.png">
  </div>
<!--
Follow me on
------------
Codepen: https://codepen.io/mycnlz/
Dribbble: https://dribbble.com/mycnlz
Pinterest: https://pinterest.com/mycnlz/
-->
<form method="post" action="login.php">
<div class='box'>
  <div class='box-form'>
    <div class='box-login-tab'></div>
    <div class='box-login-title'>
      <div class='i i-login'></div><h2 id='Kanit' style="font-size:16px;">เข้าสู่ระบบ</h2>
    </div>
    <div class='box-login'>
      <div class='fieldset-body' id='login_form'>
        	<p class='field'>
          <label for='user' id='Kanit' style="font-size:18px;">ชื่อผู้ใช้</label>
          <input type='text' id='Kanit' name='username' title='Username' required />
          <span id='valida' class='i i-warning'></span>
        </p>
      	  <p class='field'>
          <label for='pass' id='Kanit' style="font-size:18px;">รหัสผ่าน</label>
          <input type='password' id='Kanit' name='password' title='Password' required />
          <span id='valida' class='i i-close'></span>
        </p>
          <?php
          include 'time_function.php';
          if(isset($_POST['username']) && isset($_POST['password'])){
          Login($_POST['username'],$_POST['password']);
        }
           ?>
      </div>
    </div>
  </div>
        <input type='submit' id='Kanit' value='LOGIN' title='เข้าสู่ระบบ' />
</div>
</form>
</div>
</body>
<script>
$(document).ready(function() {
    $("#do_login").click(function() {
       closeLoginInfo();
       $(this).parent().find('span').css("display","none");
       $(this).parent().find('span').removeClass("i-save");
       $(this).parent().find('span').removeClass("i-warning");
       $(this).parent().find('span').removeClass("i-close");

        var proceed = true;
        $("#login_form input").each(function(){

            if(!$.trim($(this).val())){
                $(this).parent().find('span').addClass("i-warning");
            	$(this).parent().find('span').css("display","block");
                proceed = false;
            }
        });

        if(proceed) //everything looks good! proceed...
        {
            $(this).parent().find('span').addClass("i-save");
            $(this).parent().find('span').css("display","block");
        }
    });

    //reset previously results and hide all message on .keyup()
    $("#login_form input").keyup(function() {
        $(this).parent().find('span').css("display","none");
    });

  openLoginInfo();
  setTimeout(closeLoginInfo, 1000);
});

function openLoginInfo() {
    $(document).ready(function(){
    	$('.b-form').css("opacity","0.01");
      $('.box-form').css("left","-37%");
      $('.box-info').css("right","-37%");
    });
}

function closeLoginInfo() {
    $(document).ready(function(){
    	$('.b-form').css("opacity","1");
    	$('.box-form').css("left","0px");
      $('.box-info').css("right","-5px");
    });
}

$(window).on('resize', function(){
      closeLoginInfo();
});

</script>
