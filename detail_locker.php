<?php
session_start();
if(empty($_SESSION['username']) && empty($_SESSION['password']))
{
  header('Location: login.php');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="shortcut icon" href="photo/main-logo.png"/>
  <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
  <style>
  #Kanit{
    font-family: 'Kanit', sans-serif;
  }
  #center{
    text-align: center;
  }
  </style>
    <br>
  <title>รายละเอียดตู้</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>

  <body style="background-color:WhiteSmoke;"><br>
  <center><img src="photo/main-logo.png" alt="Sun108" width="270" height="250"></center>
      <br><h2 align = 'center' id='Kanit' style="margin-right:2.5%;font-size:55px;font-weight:bold;"><u>ข้อมูล</u></h2><br>
<?php
// header("Refresh:1");
include('time_function.php');
$lock = $_GET['lock'];
$number = $_GET['number'];
$locker = $lock.$number;
echo "<table class=\"table\" align = 'center' style=\"font-size:30px; width: 50%;\">";
echo "<tr class=\"table-primary\"><td style=\"text-align:right; width:48%;\" id ='Kanit'>ช่องที่&nbsp;&nbsp;&nbsp;&nbsp;: </td><td id = 'Kanit'>&nbsp;&nbsp;".$locker."</td><tr>";
require('connect.php');
$selectlock = $con->query("SELECT * FROM locker  WHERE locker.lock = '$lock' AND locker.number = '$number'");
$row = $selectlock->fetch();
$id = $row['id'];
date_default_timezone_set("Asia/Bangkok");
$_month_name = array("01"=>"มกราคม",  "02"=>"กุมภาพันธ์",  "03"=>"มีนาคม",
"04"=>"เมษายน",  "05"=>"พฤษภาคม",  "06"=>"มิถุนายน",
"07"=>"กรกฎาคม",  "08"=>"สิงหาคม",  "09"=>"กันยายน",
"10"=>"ตุลาคม", "11"=>"พฤศจิกายน",  "12"=>"ธันวาคม");
$vardate=date($row['datetimes']);
$dates =  strtotime($vardate);
$yy = date('Y',$dates);
$mm = date('m',$dates);
$dd = date('d',$dates);
$hh = date('H',$dates);
$ii = date('i',$dates);
$ss = date('s',$dates);
if ($dd<10){
$dd=substr($dd,1,2);
}
if(isset($row['datetime_stop']))
{
  $timediff = $row['time_diff'];
}
else if (empty($row['datetime_stop']))
{
  date_default_timezone_set("Asia/Bangkok");
  $ddd = strtotime(date("Y-m-d H:i:s")) - strtotime(date($row['datetime_start']));
  $timediff = $row['time_diff'] + $ddd ;
}
echo "<tr><td style=\"text-align:right\" id ='Kanit'>รหัสตู้&nbsp;&nbsp;&nbsp;&nbsp;: </td><td id = 'Kanit'>&nbsp;&nbsp;".$row['id']."</td><tr>";
echo "<tr class=\"table-primary\"><td style=\"text-align:right\" id ='Kanit'>ประเภทตู้&nbsp;&nbsp;&nbsp;&nbsp;: </td><td id = 'Kanit'>&nbsp;&nbsp;".$row['id_type']."</td><tr>";
echo "<tr><td style=\"text-align:right\" id ='Kanit'>ว/ด/ป ที่เริ่ม&nbsp;&nbsp;&nbsp;&nbsp;: </td><td id = 'Kanit'>&nbsp;&nbsp;".$date=$dd ." ".$_month_name[$mm]."  ".$yy+= 543;
echo "</td><tr>";
echo "<tr class=\"table-primary\"><td style=\"text-align:right\" id ='Kanit'>เวลาเริ่ม&nbsp;&nbsp;&nbsp;&nbsp;: </td><td id = 'Kanit'>&nbsp;&nbsp;".$hh.":".$ii.":".$ss."</td><tr>";
echo "<tr><td style=\"text-align:right\" id ='Kanit'>จำนวนเวลาที่ใช้&nbsp;&nbsp;&nbsp;&nbsp;: </td><td id = 'Kanit'>&nbsp;&nbsp;".diff2time($timediff)."</td><tr>";
echo "</table>";
//สร้างตัวแปรมาเก็บค่าที่อุณหภูมิแต่ละ sensor ที่ดึงมาจาก database
$value1 = array();
$value2 = array();
$value3 = array();
$value4 = array();
$avg = array();
$reading_time = array();
//
$sensordata = $con->query("SELECT * FROM sensordata WHERE sensordata.lock = '$lock' AND sensordata.number = '$number'");
while($fetchsensor = $sensordata->fetch())
{
	date_default_timezone_set("Asia/Bangkok");
  $time_diff=strtotime($fetchsensor['reading_time']);
	$hh = date('H',$time_diff);
	$ii = date('i',$time_diff);
	$times = $hh.":".$ii." น.";
	array_push($value1,$fetchsensor['value1']);
	array_push($value2,$fetchsensor['value2']);
	array_push($value3,$fetchsensor['value3']);
	array_push($value4,$fetchsensor['value4']);
  array_push($avg,$fetchsensor['avg']);
	array_push($reading_time,$times);
  //เก็บค่าแต่ละค่า ใส่เป็น array เพื่อนำข้อมูลไปพอทกราฟ
}
?>
      <br><br>
<center style="margin-right:2.5%;">
  <form action = 'finish.php?lock=<?=$lock;?>&number=<?=$number;?>' method="post">
    <input type="hidden" name="id" value="<?= $row['id'] ;?>"/>
    <input type="hidden" name="datetimes" value="<?= $row['datetimes'] ;?>"/>
    <input type="hidden" name="id_type" value="<?= $row['id_type'] ;?>"/>
    <input type="hidden" name="timediff" value="<?= $timediff ;?>"/>
    <input type="submit" class="btn btn-success" style="height: 50px; width: 150px; font-size: 27px;" id="Kanit" value="เสร็จสิ้น"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?php
    $id_type = $row['id_type'];
    if(isset($row['datetime_stop']))
    {
      echo "<input type=\"button\" class=\"btn btn-primary\" id=\"Kanit\" value=\"เริ่ม\" style=\"height: 50px; width: 150px; font-size: 27px;\" onclick=\"window.location.href='timestart.php?lock=$lock&number=$number&id=$id&id_type=$id_type'\"/>&nbsp;&nbsp";
    }
    else {
      echo "<input type=\"button\" class=\"btn btn-warning\" id=\"Kanit\" value=\"หยุดเวลา\" style=\"height: 50px; width: 150px; font-size: 27px;\"  onclick=\"window.location.href='timestop.php?lock=$lock&number=$number&id=$id&id_type=$id_type'\"/>&nbsp;&nbsp";
    }
    ?>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="button" class="btn btn-default" style="height: 50px; width: 150px; font-size: 27px; background-color: lightgrey;" id="Kanit" value="ย้อนกลับ" onclick='window.location.href="index.php"'/>
  </form>
</center>
<br><br>
<!-- code การแสดงกราฟ -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<canvas id="line-chart" width="300" height="100"></canvas>
<script>
new Chart(document.getElementById("line-chart"), {
type: "line",
data: {
labels: <?php echo json_encode($reading_time);?>,
datasets: [
  {
    data: <?php echo json_encode($value1, JSON_NUMERIC_CHECK) ?>,
    label: "Value 1",
    borderColor: "#3e95cd",
    fill: false
  },
  {
    data: <?php echo json_encode($value2, JSON_NUMERIC_CHECK) ?>,
    label: "Value 2",
    borderColor: "#8e5ea2",
    fill: 0
  },
  {
    data: <?php echo json_encode($value3, JSON_NUMERIC_CHECK) ?>,
    label: "Value 3",
    borderColor: "#3cba9f",
    fill: false
  },
  {
    data: <?php echo json_encode($value4, JSON_NUMERIC_CHECK) ?>,
    label: "Value4",
    borderColor: "#e8c3b9",
    fill: false
  },
  {
    data: <?php echo json_encode($avg, JSON_NUMERIC_CHECK) ?>,
    label: "อุณหภูมิเฉลี่ย",
    borderColor: "#c45850",
    fill: false
  }
]
},
options: {
title: {
  display: true,
  text: "กราฟแสดงอุณหูมิ (°C)"
}
}
});
</script>
</br></br>

</body>
