<?php
session_start();
if(empty($_SESSION['username']) && empty($_SESSION['password']))
{
  header('Location: login.php');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
  <style>
  #Kanit{
    font-family: 'Kanit', sans-serif;
  }
  #center{
    text-align: center;
  }
  .table tr:hover {background-color: #ddd;}
  .table th {
  padding-top: 12px;
  padding-bottom: 12px;
  background-color:#CC0000;
  color: white;
  }
  </style>
  <title>ประวัติ</title>
  <link rel="shortcut icon" href="photo/main-logo.png" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"></head>

  <body style="background-color:WhiteSmoke;"><br>
  <h2 align = 'center' id='Kanit' style="padding-right:5%;font-size:55px;"><u><b>ประวัติ</b></u></h2><br><br>
  <form action = 'showhistory.php' method="post">
  <p id='Kanit' align = 'center' style="font-size:20px;">เลือกวันที่จะค้นหา :&nbsp;
    <input type='date' name= 'date' required/>
    <input type="submit" id='Kanit' class="btn btn-defult" value="ค้นหา"/>
    <input type="button" id='Kanit' class="btn btn-defult" value="ประวัติทั้งหมด" onclick = "window.location.href='showhistory.php?all=1'"/>
    <input type="button" id='Kanit' class="btn btn-defult" value="ประวัติการกดหยุด/เริ่มเวลา" onclick = "window.location.href='showhistorystoptime.php'"/>
    <input type="button" id='Kanit' class="btn btn-defult" value="สถิติรายเดือน" onclick = "window.location.href='showhistorypermonth.php'"/>
    <input type="button" id='Kanit' class="btn btn-defult" value="สถิติรายปี" onclick = "window.location.href='showhistoryperyear.php'"/>
    <input type="button" class = 'btn btn-danger' id='Kanit' value="ย้อนกลับ"  onclick="window.location.href = 'index.php'"/></p>
  </form>
  <?php
  include('time_function.php');
  require('connect.php');
  if (isset($_GET['all']))
  {
    $stmt = $con->query("SELECT * FROM statistics WHERE datetime_out != 'NULL'");
    $rowcount = $stmt->rowCount();
    table($stmt,$rowcount);
  }
  else if(empty($_POST['date']))
  {
  $_month_name = array("01"=>"มกราคม",  "02"=>"กุมภาพันธ์",  "03"=>"มีนาคม",
  "04"=>"เมษายน",  "05"=>"พฤษภาคม",  "06"=>"มิถุนายน",
  "07"=>"กรกฎาคม",  "08"=>"สิงหาคม",  "09"=>"กันยายน",
  "10"=>"ตุลาคม", "11"=>"พฤศจิกายน",  "12"=>"ธันวาคม");
  $date = date("Y-m-d");
  $dates =  strtotime($date);
  $y1 = date('Y',$dates);
  $m1 = date('m',$dates);
  $d1 = date('d',$dates);
  $h1 = date('H',$dates);
  $i1 = date('i',$dates);
  $s1 = date('s',$dates);
  $yy1 = $y1 +543;
  if ($d1<10){
  $d1=substr($d1,1,2);
  }
  $stmt = $con->query("SELECT * FROM statistics WHERE datetime_out != 'NULL'  AND datetime_out LIKE '%$date%'");
  $rowcount = $stmt->rowCount();
  echo "<p id='Kanit' align = 'center' style='font-size:20px;'>วันที่"
  ." ".$d1 ." ".$_month_name[$m1]."  ".$yy1."</p>";
  table($stmt,$rowcount);
  }
  else {
    $_month_name = array("01"=>"มกราคม",  "02"=>"กุมภาพันธ์",  "03"=>"มีนาคม",
    "04"=>"เมษายน",  "05"=>"พฤษภาคม",  "06"=>"มิถุนายน",
    "07"=>"กรกฎาคม",  "08"=>"สิงหาคม",  "09"=>"กันยายน",
    "10"=>"ตุลาคม", "11"=>"พฤศจิกายน",  "12"=>"ธันวาคม");
    $date = $_POST['date'];
    $dates =  strtotime($date);
    $y1 = date('Y',$dates);
    $m1 = date('m',$dates);
    $d1 = date('d',$dates);
    $h1 = date('H',$dates);
    $i1 = date('i',$dates);
    $s1 = date('s',$dates);
    $yy1 = $y1 +543;
    if ($d1<10){
    $d1=substr($d1,1,2);
    }
    $stmt = $con->query("SELECT * FROM statistics WHERE datetime_out != 'NULL' AND datetime_out LIKE '%$date%'");
    $rowcount = $stmt->rowCount();
    echo "<p id='Kanit' align = 'center' style='font-size:20px;'>วันที่"." ".$d1 ." ".$_month_name[$m1]."  ".$yy1."</p>";
    table($stmt,$rowcount);
  }
  ?>
